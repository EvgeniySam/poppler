#ifndef THREADSEARCH_H
#define THREADSEARCH_H

#include <QThread>
#include "poppler/qt5/poppler-qt5.h"
#include "page.h"

class ThreadSearch : public QThread
{
    Q_OBJECT
public:
    explicit ThreadSearch(QObject *parent = nullptr);
    void setListPages(QList<Page *> &_listPages);
    void setSearchParams(QList<Page *> &_listPages, QString _text);

protected:
    void run() override;
signals:
    void finished();
private:
    QList<Page*> m_listPages;
    QString m_stringSearch;
};

#endif // THREADSEARCH_H
