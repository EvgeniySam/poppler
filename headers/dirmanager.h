#ifndef DIRMANAGER_H
#define DIRMANAGER_H

#include <QObject>
#include <QFileSystemWatcher>
#include "fileinfo.h"

class DirManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int sessionType MEMBER m_currentType)
    Q_PROPERTY(QList<QObject*> listRoutes READ listRoutes NOTIFY listRoutesChanged)
    Q_PROPERTY(QList<QObject*> listRegularDocuments READ listRegularDocuments NOTIFY listRegularDocumentsChanged)
    Q_PROPERTY(QList<QObject*> listRaports READ listRaports NOTIFY listRaportsChanged)
public:
    enum SessionType{
        RegulatoryDocuments,
        RouteDisplay,
        RaportsDisplay,
        ActiveRout
    };

    Q_ENUM(SessionType)

    explicit DirManager(QObject *parent = nullptr);
    Q_INVOKABLE QString currentRoutName();
    static void setCurrentType(SessionType newCurrentType);
    static QString dirPath();
    static void setCurrentRoutName(const QString &newCurrentRoutName);
    static int currentType();
    QList<QObject*> listRoutes() const;
    QList<QObject*> listRegularDocuments() const;
    QList<QObject*> listRaports() const;
signals:
    void listRegularDocumentsChanged();
    void listRoutesChanged();
    void listRaportsChanged();

private:
    void initRaports();
    void initRoutes();
    void initRegulatoryDocuments();
    static int m_currentType;
    static QString m_currentRoutName;
    QList<FileInfo*> m_routes;
    QList<FileInfo*> m_regulatoryDocuments;
    QList<FileInfo*> m_raports;
    QFileSystemWatcher m_watcher;
};

#endif // DIRMANAGER_H
