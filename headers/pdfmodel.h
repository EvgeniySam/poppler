#ifndef PDFMODEL_H
#define PDFMODEL_H

#include <QObject>
#include <QVariantList>
#include <imageprovider.h>
#include "poppler/qt5/poppler-qt5.h"
#include "threadsearch.h"
#include "page.h"
#include <QObjectList>

struct Transfer{
    Q_GADGET
public:
    Transfer(int _page = -1, double _x = 0, double _y = 0,
             double _width = 0, double _height = 0):
        m_page(_page),
        m_x(_x),
        m_y(_y),
        m_width(_width),
        m_height(_height)
    {

    };
private:
    int m_page;
    double m_x;
    double m_y;
    double m_width;
    double m_height;
    Q_PROPERTY(int page MEMBER m_page)
    Q_PROPERTY(double x MEMBER m_x)
    Q_PROPERTY(double y MEMBER m_y)
    Q_PROPERTY(double width MEMBER m_width)
    Q_PROPERTY(double height MEMBER m_height)
};

class PDFModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QList<QObject*> pages READ pages NOTIFY pagesChanged)
    Q_PROPERTY(int pagesCount READ pagesCount NOTIFY pagesCountChanged)

public:
    explicit PDFModel(QObject *parent = nullptr);
    QList<QObject*> pages() const;
    Q_INVOKABLE void search(const QString& text, Qt::CaseSensitivity caseSensitivity = Qt::CaseInsensitive);
    Q_INVOKABLE Transfer nextRect();
    Q_INVOKABLE Transfer previousRect();
    Q_INVOKABLE void clearSearch();

    QString path() const;
    void setPath(QString& pathName);
    void loadProvider();
    int pagesCount();
public slots:
    void fillVectorIndex();
signals:
    void pagesChanged();
    void pathChanged();
    void loadedChanged();
    void pagesCountChanged();
    void searchResultChanged();
    void searchFinished();

private:
    QVector<int>::Iterator it;
    ThreadSearch m_threadSearch;
    QList<Page*> m_pages;
    QVector<int> m_pageIndex;
    QVector<QVariantList> searchOnPage;
    QVector<QList<QRectF>> m_allPageAllRect;
    QString m_path;
    Poppler::Document *m_document = nullptr;
    QString m_providerName;
};
Q_DECLARE_METATYPE(Transfer)
#endif // PDFMODEL_H
