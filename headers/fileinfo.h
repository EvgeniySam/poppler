#ifndef FILEINFO_H
#define FILEINFO_H

#include <QObject>

class FileInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nameFile READ nameFile NOTIFY non)
    Q_PROPERTY(QString pathFile READ pathFile NOTIFY non)

public:
    explicit FileInfo(QString name, QString path, QObject *parent = nullptr);
    QString nameFile();
    QString pathFile();

private:
    QString m_nameFile;
    QString m_pathFile;
signals:
    void non();
};

#endif // FILEINFO_H
