#ifndef CONFIGS_H
#define CONFIGS_H

#include <QObject>
#include <QFont>
#include <QColor>
#include <QtQml>

class Configs : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QFont droid30Font MEMBER m_30Font NOTIFY font30Updated)
    Q_PROPERTY(QFont droid24Font MEMBER m_24Font NOTIFY font24Updated)
    Q_PROPERTY(QFont droid24BoldFont MEMBER m_24BoldFont NOTIFY font24BoldUpdated)
    Q_PROPERTY(QFont droid20Font MEMBER m_20Font NOTIFY font20Updated)
    Q_PROPERTY(QFont droid28Font MEMBER m_28Font NOTIFY font28Updated)
    Q_PROPERTY(QFont droid28BoldFont MEMBER m_28BoldFont NOTIFY font28BoldUpdated)
    Q_PROPERTY(QFont droid36Font MEMBER m_36Font NOTIFY font36Updated)
    Q_PROPERTY(QFont droid48Font MEMBER m_48Font NOTIFY font48Updated)
    Q_PROPERTY(QColor foregroundColor MEMBER m_foregroundColor NOTIFY foregroundColorUpdated)
    Q_PROPERTY(QColor backgroundColor MEMBER m_backgroundColor NOTIFY backgroundColorUpdated)
    Q_PROPERTY(QColor accentColor MEMBER m_accentColor NOTIFY accentColorUpdated)
    Q_PROPERTY(QColor waringColor MEMBER m_waringColor NOTIFY waringColorUpdated)
    Q_PROPERTY(QColor assuageColor MEMBER m_assuageColor NOTIFY assuageColorUpdated)
    Q_PROPERTY(QColor disabledColor MEMBER m_disabledColor NOTIFY disabledColorUpdated)
    Q_PROPERTY(QColor disabledBackgroundColor MEMBER m_disabledBackgroundColor NOTIFY disabledBackgroundColorUpdated)
    Q_PROPERTY(QColor selectedItemColor MEMBER m_selectedItemColor NOTIFY selectedItemColorUpdated)
    Q_PROPERTY(double scale MEMBER m_scale CONSTANT)
    Q_PROPERTY(QString userName READ userName NOTIFY userNameChanged)

public:
    static constexpr double m_scale = 0.5;
    explicit Configs(QObject *parent = nullptr);
    static QString DirRoutes;
    static QString DirRegulatoryDocuments;
    static QString DirRaports;
    static QString logPath;
    QString userName();

signals:
    void font30Updated();
    void font24Updated();
    void font24BoldUpdated();
    void font20Updated();
    void font28Updated();
    void font28BoldUpdated();
    void font36Updated();
    void font48Updated();
    void userNameChanged();
    void foregroundColorUpdated();
    void backgroundColorUpdated();
    void accentColorUpdated();
    void waringColorUpdated();
    void assuageColorUpdated();
    void disabledColorUpdated();
    void disabledBackgroundColorUpdated();
    void selectedItemColorUpdated();

private:
    QFont m_30Font;
    QFont m_24Font;
    QFont m_24BoldFont;
    QFont m_20Font;
    QFont m_28Font;
    QFont m_36Font;
    QFont m_48Font;
    QFont m_28BoldFont;
    QColor m_foregroundColor;
    QColor m_backgroundColor;
    QColor m_accentColor;
    QColor m_waringColor;
    QColor m_assuageColor;
    QColor m_disabledColor;
    QColor m_disabledBackgroundColor;
    QColor m_selectedItemColor;
    QString m_username;
    static void loadSetting();

};
#endif // CONFIGS_H
