#ifndef PAGE_H
#define PAGE_H

#include <QObject>
#include <QSizeF>
#include "poppler/qt5/poppler-qt5.h"

class Page : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString imagePath READ imagePath NOTIFY imagePathChanged)
    Q_PROPERTY(QSizeF size READ sizeImage NOTIFY sizeImageChanged)
public:
    explicit Page(Poppler::Page* _page, QObject *parent = nullptr);
    Page(QObject *parent = nullptr);
    const QString &imagePath() const;
    void setImagePath(const QString &newImagePath);
    const QSizeF sizeImage();
    void searchOnPage(const QString& _text);
    int countFoundRect() const;
    QRectF nextRect();
    QRectF previousRect();
    void clear();
signals:
    void imagePathChanged();
    void sizeImageChanged();
private:
  QList<QRectF> m_foundRectanglesList;
  int m_currentRectIndex = -1;
  QList<QRectF>::Iterator itEnd;
  Poppler::Page* m_page = nullptr;
  QString m_imagePath;
};

#endif // PAGE_H
