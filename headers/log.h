#ifndef QMLLOGGING_H
#define QMLLOGGING_H

#include <QLoggingCategory>
#include <QDebug>
#include <QObject>

Q_DECLARE_LOGGING_CATEGORY(APPLICATION)    // События в работе СПО: запуск и закрытие приложений, и т.п.
Q_DECLARE_LOGGING_CATEGORY(PDF_VIEW)       // Просмотр pdf-файлов внутри СПО.

#define REGISTR_LOGGER(name,event)\
    Q_INVOKABLE void name##Success(const QString &str){\
        qCDebug(event).noquote () << str; \
    }\
    Q_INVOKABLE void name##Info(const QString &str){\
        qCInfo(event).noquote () << str; \
    }\
    Q_INVOKABLE void name##Error(const QString &str){\
        qCCritical(event).noquote () << str; \
    }\
    Q_INVOKABLE void name##Warning(const QString &str){\
        qCWarning(event).noquote () << str; \
    }\

class QmlLogging : public QObject
{
    Q_OBJECT
public:
    explicit QmlLogging(QObject *parent = nullptr);
    REGISTR_LOGGER(application, APPLICATION)
    REGISTR_LOGGER(pdf_view,PDF_VIEW)
};

#endif // QMLLOGGING_H
