#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <QQuickImageProvider>
#include <poppler/qt5/poppler-qt5.h>

class ImageProvider : public QQuickImageProvider
{
public:
    ImageProvider(Poppler::Document* pdfDocument = nullptr);
    QImage requestImage(const QString& id, QSize* size, const QSize& requestedSize);
private:
  Poppler::Document *m_document;
};

#endif // IMAGEPROVIDER_H
