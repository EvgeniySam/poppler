import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import ArmyPatrol 1.0
import QtQuick.VirtualKeyboard 2.1

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 800 //* configs.scale
    height: 1280 //* configs.scale
    flags: Qt.FramelessWindowHint
    Material.accent: configs.accentColor
    Material.foreground: configs.foregroundColor
    Material.background: configs.backgroundColor
    menuBar: GridLayout {
        id: headerBar
        columns: 1
        columnSpacing: 0
        rowSpacing: 0
        Rectangle {
            id: titleBar
            width: mainWindow.width
            height: 70 * configs.scale
            color: "#E0DEDE"
            Text {
                font.pixelSize: 30 * configs.scale
                anchors.centerIn: parent
                text: "PDF view"
            }
        }
        Header {
            id: headerBarElement
            width: mainWindow.width
            onBackClicked: {
                webView.visible = false
                list.visible = true
                headerBarElement.backTextVisible = false
            }
        }
    }
    Config{id: configs}
    QmlLogging{ id: log}

    PagePdf{
        id: pdfPage
        z: 2
        anchors.fill: parent
        visible: false

    }
    DirManager{ id:dirManager }

    InputPanel {
        id: inputPanel
        z: 100
        y: Qt.inputMethod.visible ? parent.height - inputPanel.height : parent.height + 50
        anchors.left: parent.left
        anchors.right: parent.right
        scale: 1.1
    }

    Text {
        id: title
        anchors.left: parent.left
        anchors.leftMargin: 41 * configs.scale
        anchors.top: parent.top
        anchors.topMargin: 41 * configs.scale
        font: configs.droid48Font
    }

    ListView{
        id: list
        anchors.left: parent.left
        anchors.leftMargin: 40 * configs.scale
        anchors.right: parent.right
        anchors.rightMargin: 40 * configs.scale
        anchors.top: title.bottom
        anchors.topMargin: 55 * configs.scale
        anchors.bottom: underline.top
        model: dirManager.listRegularDocuments
        anchors.bottomMargin: 10 * configs.scale
        delegate: Rectangle{
            anchors.right: parent.right
            anchors.left: parent.left
            height: namePDF.contentHeight + 20 * configs.scale
            Text {
                id: numberPDF
                text: index + 1
                font: configs.droid36Font
                anchors.left: parent.left
                width: 50 * configs.scale
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
            }
            Text {
                id: namePDF
                text: modelData.nameFile
                anchors.right: parent.right
                anchors.rightMargin: 10 * configs.scale
                anchors.left: numberPDF.right
                anchors.leftMargin: 20 * configs.scale
                anchors.topMargin: 20 * configs.scale
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20 * configs.scale
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.Wrap
                font: configs.droid36Font
            }
            Rectangle{
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: 1
                color: configs.foregroundColor
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    pdfPage.pdfPath = modelData.pathFile
                    pdfPage.visible = true
                    log.pdf_viewSuccess("Пользователь " + configs.userName + " открыл на чтение "
                                        + modelData.pathFile + modelData.nameFile)
                }
            }
        }
    }

    Rectangle{
        id: underline
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 133 * configs.scale
        height: 1
        color: configs.foregroundColor
    }

}

