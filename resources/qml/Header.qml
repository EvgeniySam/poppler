﻿import QtQuick 2.0
import QtGraphicalEffects 1.0
import ArmyPatrol 1.0

Rectangle {
    id: control
    height: 200 * configs.scale
    color: "#4D4D4D"
    property alias backTextVisible: backText.visible
    function setElementVisible(visible) {
        for(var i in this.children) {
            if(i !== "0")
                this.children[i].visible = visible
        }
    }
    function hideElemets() { setElementVisible(false) }
    function showElemets() { setElementVisible(true) }

    signal backClicked()


    Text {
        id: backText
        visible: false
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 108 * configs.scale
        anchors.topMargin: 31 * configs.scale
        color: "#FFFFFF"
        font.pixelSize: 36 * configs.scale
        text: "< Назад"
        MouseArea{
            anchors.fill: parent
            onClicked: backClicked()
        }
    }

    Image {
        source: "/img/exit.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 50 * configs.scale
        sourceSize: Qt.size(60 * configs.scale, 60 * configs.scale)
        MouseArea {
            anchors.fill: parent
            anchors.topMargin: -20 * configs.scale
            anchors.bottomMargin:  -20 * configs.scale
            anchors.leftMargin: -20 * configs.scale
            anchors.rightMargin: -20 * configs.scale
            onClicked: Qt.quit()
        }
    }
}
