import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import ArmyPatrol 1.0
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    property alias pdfPath: pdfModel.path
    property var rectFind: null

    function destroyRect(){
        if(rectFind != null){
            rectFind.destroy()
            rectFind = null
        }
    }

    function createRect(x, y, width, height){
        var component = Qt.createComponent("FindRect.qml");
        if (component.status == Component.Ready) {
            rectFind = component.createObject(pagesView.currentItem.imagePage);
            rectFind.color = "green";
            rectFind.opacity = 0.3;
            rectFind.x = Math.round(x * pagesView.currentItem.imagePage.width)
            rectFind.y = Math.round(y * pagesView.currentItem.imagePage.height)
            rectFind.width = Math.round(width * pagesView.currentItem.imagePage.width)
            rectFind.height = Math.round(height * pagesView.currentItem.imagePage.height)
        }
    }

    color: configs.backgroundColor

    Image {
        id: imgUp
        anchors.left: parent.left
        anchors.leftMargin: 40 * configs.scale
        anchors.top: parent.top
        anchors.topMargin: 40 * configs.scale
        source: "qrc:/img/ButtonUp.svg"
        fillMode: Image.PreserveAspectCrop
        width: 90 * configs.scale
        height: 60 * configs.scale
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(pagesView.currentIndex != 0){
                    pagesView.currentIndex--;
                }
            }
        }
    }
    ColorOverlay{
        id: colorUp
        anchors.fill: imgUp
        source: imgUp
        color: configs.foregroundColor
    }

    Image {
        id: imgDown
        anchors.left: imgUp.right
        anchors.leftMargin: 40 * configs.scale
        anchors.verticalCenter: imgUp.verticalCenter
        source: "qrc:/img/ButtonDown.svg"
        fillMode: Image.PreserveAspectCrop
        width: 90 * configs.scale
        height: 60 * configs.scale
        ColorOverlay{
            id: colorDown
            anchors.fill: imgDown
            source: imgDown
            color: configs.foregroundColor
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(pagesView.currentIndex != pdfModel.pagesCount-1){
                    pagesView.currentIndex++;
                }
            }
        }
    }

    Rectangle{
        id: rectPageInput
        width: 120 * configs.scale
        height: 100 * configs.scale
        anchors.verticalCenter: imgUp.verticalCenter
        anchors.left: imgDown.right
        anchors.leftMargin: 40 * configs.scale
        border.color: configs.foregroundColor
        border.width: 1
        TextField{
            id: pageInput
            verticalAlignment: TextInput.AlignVCenter
            horizontalAlignment: TextInput.AlignHCenter
            inputMethodHints: Qt.ImhNoPredictiveText
            anchors.fill: parent
            font: configs.droid36Font
            maximumLength: String(pdfModel.pagesCount).length
            onTextEdited: {

            }
            Keys.onPressed: {
                if(event.key == Qt.Key_Return){
                    if(text == 1){
                        colorUp.color = "lightgrey"
                    }else{
                        colorUp.color = configs.foregroundColor
                    }
                    if(text == pdfModel.pagesCount){
                        colorDown.color = "lightgrey"
                    }else{
                        colorDown.color = configs.foregroundColor
                    }
                    pagesView.currentIndex = text - 1
                }
            }
        }
    }

    Text {
        id: textPageCount
        font: configs.droid36Font
        anchors.left: rectPageInput.right
        anchors.leftMargin: 14 * configs.scale
        anchors.verticalCenter: rectPageInput.verticalCenter
        text: "из " + pdfModel.pagesCount
    }

    Image {
        id: zoom
        source: "qrc:/img/Zoom.svg"
        height: 80 * configs.scale
        width: height
        fillMode: Image.PreserveAspectFit
        anchors.left: textPageCount.right
        anchors.leftMargin: 20 * configs.scale
        anchors.verticalCenter: textPageCount.verticalCenter
        ColorOverlay{
            anchors.fill: zoom
            source: zoom
            color: configs.foregroundColor
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                popupDialog.open()
            }
        }
    }

    Rectangle{
        id: rectFindInput
        width: 350 * configs.scale
        height: 90 * configs.scale
        anchors.verticalCenter: imgUp.verticalCenter
        anchors.left: zoom.right
        anchors.leftMargin: 20 * configs.scale
        border.color: configs.foregroundColor
        border.width: 1
        clip: true
        TextField{
            id: textFind
            anchors.fill: parent
            clip: true
            font: configs.droid36Font
            inputMethodHints: Qt.ImhNoPredictiveText
        }
    }
    Rectangle{
        id: buttonFind
        anchors.left: rectFindInput.right
        anchors.verticalCenter: rectFindInput.verticalCenter
        height: rectFindInput.height
        width: 150 * configs.scale
        border.color: configs.foregroundColor
        border.width: 1
        color: configs.assuageColor
        Text {
            anchors.fill: parent
            text: "Поиск"
            font: configs.droid36Font
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                var str = textFind.text
                if( str.trim() != ""){
                    dialogProgressFind.visible = true
                    dusyIndicator.running = true
                    pdfModel.search(textFind.text)
                }
            }
        }
    }

    Rectangle{
        id: next
        width: 100 * configs.scale
        height: 100 * configs.scale
        anchors.verticalCenter: buttonFind.verticalCenter
        anchors.left: buttonFind.right
        anchors.leftMargin: 30 * configs.scale
        visible: false
        Image {
            anchors.fill: parent
            source: "qrc:/img/nextfind.svg"
            fillMode: Image.PreserveAspectFit
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                var structWithRectangleParameters = pdfModel.nextRect()
                if(pagesView.currentIndex != structWithRectangleParameters.page){
                    pagesView.currentIndex = structWithRectangleParameters.page
                }
                destroyRect()
                createRect(structWithRectangleParameters.x,
                           structWithRectangleParameters.y,
                           structWithRectangleParameters.width,
                           structWithRectangleParameters.height)
            }
        }
    }
    Rectangle{
        id: previous
        width: 100 * configs.scale
        height: 100 * configs.scale
        anchors.verticalCenter: buttonFind.verticalCenter
        anchors.left: next.right
        anchors.leftMargin: 30 * configs.scale
        visible: false
        Image {
            anchors.fill: parent
            source: "qrc:/img/prevfind.svg"
            fillMode: Image.PreserveAspectFit
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                var structWithRectangleParameters = pdfModel.previousRect()
                if(pagesView.currentIndex != structWithRectangleParameters.page){
                    pagesView.currentIndex = structWithRectangleParameters.page
                }
                destroyRect()
                createRect(structWithRectangleParameters.x,
                           structWithRectangleParameters.y,
                           structWithRectangleParameters.width,
                           structWithRectangleParameters.height)
            }
        }
    }

    Rectangle{
        id: clearFind
        width: 100 * configs.scale
        height: 100 * configs.scale
        anchors.verticalCenter: buttonFind.verticalCenter
        anchors.left: previous.right
        anchors.leftMargin: 30 * configs.scale
        visible: false
        Image {
            anchors.fill: parent
            source: "qrc:/img/clear.svg"
            fillMode: Image.PreserveAspectFit
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                textFind.clear()
                pdfModel.clearSearch()
                destroyRect()
                next.visible = false
                previous.visible = false
                clearFind.visible = false
            }
        }
    }

    Popup{
        id: popupDialog
        width: 500 * configs.scale
        height: 120 * configs.scale
        x: webView.x + (40 * configs.scale)
        y: webView.y
        Slider{
            id: slider
            scale: 1.2
            x: popupDialog.x
            from: 0.2
            to: 4
            value: 0.5
        }
    }

    Rectangle{
        id: dialogProgressFind
        anchors.fill: parent
        z: 100
        visible: false
        anchors.centerIn: parent
        color: "#00000000"
        BusyIndicator{
            id: dusyIndicator
            anchors.centerIn: parent
            width: 500 * configs.scale
            height: 500 * configs.scale
            running: false
        }
        MouseArea{
            anchors.fill: parent
        }

    }


    Rectangle {
        id: webView
        anchors.top: buttonFind.bottom
        anchors.topMargin: 30 * configs.scale
        anchors.bottom: buttonBack.top
        anchors.bottomMargin: 26 * configs.scale
        anchors.left: parent.left
        anchors.right: parent.right
        color: configs.foregroundColor
        clip: true
        PdfModel{
            id: pdfModel
            onSearchFinished: {
                dialogProgressFind.visible = false
                dusyIndicator.running = false
                next.visible = true
                previous.visible = true
                clearFind.visible = true
                var structWithRectangleParameters = pdfModel.nextRect()
                if(pagesView.currentIndex != structWithRectangleParameters.page){
                    pagesView.currentIndex = structWithRectangleParameters.page
                }
                createRect(structWithRectangleParameters.x,
                           structWithRectangleParameters.y,
                           structWithRectangleParameters.width,
                           structWithRectangleParameters.height)
            }
        }

        ListView {
            id: pagesView
            spacing: 20
            anchors.fill: parent
            boundsBehavior: Flickable.StopAtBounds
            model: pdfModel.pages
            header: Item {
                height: 20
            }
            footer: Item {
                height: 20
            }
            highlightMoveDuration: 1000
            highlightMoveVelocity: 1
            delegate:Flickable{
                id: flickable
                property var imagePage: pageImage
                property int pageNumber: index
                flickableDirection: Flickable.HorizontalFlick
                width: webView.width
                height: content.height
                ScrollBar.vertical: ScrollBar{}
                contentHeight: Math.max(content.height, webView.height)
                contentWidth: Math.max(content.width, webView.width)
                Item {
                    id: content
                    z: 3
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: pageImage.width
                    height: pageImage.height
                    Image {
                        id: pageImage
                        x: Math.round((parent.width - sourceSize.width) / 2)
                        cache: false
                        fillMode: Image.Pad
                        sourceSize.width: Math.round(modelData.size.width * slider.value)
                        sourceSize.height: Math.round(modelData.size.height * slider.value)
                        source: modelData.imagePath
                        width: sourceSize.width
                        height: sourceSize.height
                        onSourceSizeChanged: {

                        }
                    }
                }
            }
            onCurrentIndexChanged: {
                pageInput.text = currentIndex + 1
            }
        }
    }

    Rectangle{
        id: buttonBack
        radius: 3
        width: 250 * configs.scale
        height: 100 * configs.scale
        color: configs.assuageColor
        border.color: "black"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 23 * configs.scale
        Text {
            text: "Закрыть"
            font: configs.droid36Font
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                log.pdf_viewSuccess("Пользователь " + configs.userName + " закрыл файл "
                                    + pdfPath)
                Qt.quit()
            }
        }
    }

    Config{ id: configs }

}
