#include "dirmanager.h"
#include <QDir>
#include "configs.h"

int DirManager::m_currentType = DirManager::RegulatoryDocuments;
QString DirManager::m_currentRoutName = "";

DirManager::DirManager(QObject *parent) : QObject(parent)
{
    switch (DirManager::currentType()) {
    case DirManager::SessionType::ActiveRout :{
        if(QDir(Configs::DirRoutes).exists(m_currentRoutName)){
            setCurrentRoutName(Configs::DirRoutes + "/" + m_currentRoutName);
        }else{
            qDebug() << "not file";
        }
        break;
    }
    case DirManager::SessionType::RaportsDisplay :{
        m_watcher.addPath(Configs::DirRaports);
        connect(&m_watcher,&QFileSystemWatcher::directoryChanged,[&](){
            initRaports();
            emit listRaportsChanged();
        });
        initRaports();
        break;
    }
    case DirManager::SessionType::RouteDisplay :{
        initRoutes();
        break;
    }
    case DirManager::SessionType::RegulatoryDocuments :{
        initRegulatoryDocuments();
        break;
    }
    }
}

QString DirManager::currentRoutName()
{
    return m_currentRoutName;
}

void DirManager::setCurrentType(SessionType newCurrentType)
{
    m_currentType = newCurrentType;
}

QString DirManager::dirPath()
{
    ///TODO Добавить пути из конфига, пока что хардкор(д)
    if(m_currentType == DirManager::RegulatoryDocuments)
        return Configs::DirRegulatoryDocuments;
    else if(m_currentType == DirManager::RaportsDisplay)
        return QDir::homePath() + "/Документы/Рапорты";
    else if(m_currentType == DirManager::RouteDisplay)
        return Configs::DirRoutes;
    else return Configs::DirRoutes + "/" + m_currentRoutName;
    ///TODO Проверку на существование файла
}

void DirManager::setCurrentRoutName(const QString &newCurrentRoutName)
{
    m_currentRoutName = newCurrentRoutName;
}

int DirManager::currentType()
{
    return m_currentType;
}

QList<QObject *> DirManager::listRoutes() const
{
    QList<QObject *> rezult;
    for(const auto it: qAsConst(m_routes)){
        rezult << it;
    }
    return rezult;
}

QList<QObject *> DirManager::listRegularDocuments() const
{
    QList<QObject *> rezult;
    for(const auto it: qAsConst(m_regulatoryDocuments)){
        rezult << it;
    }
    return rezult;
}

QList<QObject *> DirManager::listRaports() const
{
    QList<QObject *> rezult;
    for(const auto it: qAsConst(m_raports)){
        rezult << it;
    }
    return rezult;
}

void DirManager::initRaports()
{
    QDir dir(Configs::DirRaports);
    qDeleteAll(m_raports);
    m_raports.clear();
    for(const auto &it: dir.entryInfoList(QDir::Files)){
        m_raports << new FileInfo(it.fileName(),it.filePath());
    }
    qDebug() << m_raports;
}

void DirManager::initRoutes(){
    QDir dir(Configs::DirRoutes);
    for(const auto &it: dir.entryInfoList(QDir::Files)){
        m_routes << new FileInfo(it.fileName(),it.filePath());
    }
}

void DirManager::initRegulatoryDocuments()
{
    QDir dir(Configs::DirRegulatoryDocuments);
    for(const auto &it: dir.entryInfoList(QDir::Files)){
        m_regulatoryDocuments << new FileInfo(it.fileName(),it.filePath());
    }
}



