#include "fileinfo.h"

FileInfo::FileInfo(QString name, QString path, QObject *parent) :
    m_nameFile(name),
    m_pathFile(path),
    QObject(parent)
{

}

QString FileInfo::nameFile()
{
    return m_nameFile;
}

QString FileInfo::pathFile()
{
    return m_pathFile;
}


