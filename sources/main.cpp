#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "configs.h"
#include "dirmanager.h"
#include "pdfmodel.h"
#include "log.h"
#include <iostream>
#include <unistd.h>
#include <memory>

std::unique_ptr<QFile> g_logFile;

Q_LOGGING_CATEGORY(APPLICATION, "APPLICATION") // События в работе СПО: запуск и закрытие приложений, и т.п.
Q_LOGGING_CATEGORY(PDF_VIEW, "PDF_VIEW")       // Просмотр pdf-файлов внутри СПО.

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
void writeLogStartApp(int Type);
void writeLogEndApp(int Type);


int main(int argc, char *argv[])
{

    qputenv ( "QT_IM_MODULE" , QByteArray ( "qtvirtualkeyboard" ));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    Configs config;

    g_logFile.reset(new QFile(Configs::logPath));
    g_logFile->open(QFile::Append | QFile::Text);
    qInstallMessageHandler(messageHandler);
    writeLogStartApp(DirManager::currentType());

    int readPeck = app.arguments().indexOf("--read-file");

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));

    qRegisterMetaType<Transfer>();
    qmlRegisterType<DirManager>("ArmyPatrol",1,0,"DirManager");
    qmlRegisterType<Configs>("ArmyPatrol",1,0,"Config");
    qmlRegisterType<PDFModel>("ArmyPatrol",1,0,"PdfModel");
    qmlRegisterType<QmlLogging>("ArmyPatrol", 1, 0, "QmlLogging");
    engine.rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    int codeExec = app.exec();
    writeLogEndApp(DirManager::currentType());
    return codeExec;
}

void printHelp() {
    std::cout << "Ключи запуска: " << std::endl
              << "\t -h, --help           " << "\tнапечатать справку и выйти" << std::endl;
}

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Открываем поток записи в файл
    QTextStream out(g_logFile.get());
    // Записываем дату записи
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ") << context.category;
    // По типу определяем, к какому уровню относится сообщение
    switch (type)
    {
    case QtInfoMsg:     out << " INFO"; break;
    case QtDebugMsg:    out << " SUCCESS"; break;
    case QtWarningMsg:  out << " WARNING"; break;
    default:
        out << " ERROR"; break;
    }
    // Записываем в вывод категорию сообщения и само сообщение
    out << ":\t" << msg << endl;
    out.flush();    // Очищаем буферизированные данные
}

void writeLogStartApp(int Type){
    Configs conf;
    switch (Type) {
    case DirManager::SessionType::RegulatoryDocuments:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " запускает приложение .";
        break;
    case DirManager::SessionType::RouteDisplay:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " запускает приложение .";
        break;
    case DirManager::SessionType::RaportsDisplay:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " запускает приложение.";
        break;
    case DirManager::SessionType::ActiveRout:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " просматривае.";
        break;
    default:
        break;
    }
}

void writeLogEndApp(int Type){
    Configs conf;
    switch (Type) {
    case DirManager::SessionType::RegulatoryDocuments:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " закрывает приложение 'Регламентирующие документы'.";
        break;
    case DirManager::SessionType::RouteDisplay:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " закрывает приложение 'Список маршрутов'.";
        break;
    case DirManager::SessionType::RaportsDisplay:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " закрывает приложение 'Список рапортов'.";
        break;
    case DirManager::SessionType::ActiveRout:
        qCDebug(APPLICATION()) << " Пользователь "
        << conf.userName()
        << " просматривает выбранный маршрут.";
        break;
    default:
        break;
    }
}
