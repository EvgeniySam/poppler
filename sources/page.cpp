#include "page.h"
#include <iostream>
#include <QDebug>

Page::Page(Poppler::Page *_page, QObject *parent) :
    m_page(_page),
    QObject(parent)
{

}

Page::Page(QObject *parent)
{

}

const QString &Page::imagePath() const
{
    return m_imagePath;
}

void Page::setImagePath(const QString &newImagePath)
{
    m_imagePath = newImagePath;
}

const QSizeF Page::sizeImage()
{
    if(m_page != nullptr)
        return m_page->pageSizeF();
    return QSizeF(0,0);
}

void Page::searchOnPage(const QString &_text)
{
    if(!m_foundRectanglesList.isEmpty())
    {
        m_foundRectanglesList.clear();
    }
    auto searchResult = m_page->search(_text, Poppler::Page::IgnoreCase);
    auto pageSize = m_page->pageSizeF();
    for (const auto& r : qAsConst(searchResult))
    {
        m_foundRectanglesList.push_back(QRectF(r.left() / pageSize.width(), r.top() / pageSize.height(), r.width() / pageSize.width(), r.height() / pageSize.height()));
    }
}

int Page::countFoundRect() const
{
    return m_foundRectanglesList.size();
}

QRectF Page::nextRect()
{
    QRectF temp;
    ++m_currentRectIndex;
    if(m_currentRectIndex < m_foundRectanglesList.size()){
        temp = m_foundRectanglesList.at(m_currentRectIndex);
    }
    return temp;
}

QRectF Page::previousRect()
{
    QRectF temp;
    if(m_currentRectIndex == -1){
        m_currentRectIndex = m_foundRectanglesList.size();
    }
    --m_currentRectIndex;
    if(m_currentRectIndex > -1){
        temp = m_foundRectanglesList.at(m_currentRectIndex);
    }
    return temp;
}

void Page::clear()
{
    m_foundRectanglesList.clear();
}



