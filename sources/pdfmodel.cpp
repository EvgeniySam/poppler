#include "pdfmodel.h"
#include <QQmlEngine>
#include <QQmlContext>
#include <memory>
#include <iostream>

PDFModel::PDFModel(QObject *parent) : QObject(parent)
{
    connect(&m_threadSearch, &ThreadSearch::finished, this, &PDFModel::fillVectorIndex);
}

static QVariantMap convertDestination(const Poppler::LinkDestination& destination)
{
    QVariantMap result;
    result["page"] = destination.pageNumber() - 1;
    result["top"] = destination.top();
    result["left"] = destination.left();
    return result;
}

QList<QObject *> PDFModel::pages() const
{
    QList<QObject*> result;
    for(auto it: m_pages){
        result.append(it);
    }
    return result;
}

QString PDFModel::path() const
{
    return m_path;
}

void PDFModel::setPath(QString &pathName)
{
    if (pathName == m_path)
        return;
    if(!m_pages.isEmpty()){
        m_pages.clear();
    }
    m_path = pathName;
    emit pathChanged();
    m_document = Poppler::Document::load(m_path);
    if(!m_document || m_document->isLocked()){
        delete m_document;
        m_document = nullptr;
        return;
    }
    m_document->setRenderHint(Poppler::Document::Antialiasing, true);
    m_document->setRenderHint(Poppler::Document::TextAntialiasing, true);
    loadProvider();
    for (int i = 0; i < m_document->numPages(); ++i){
        Page *page = new Page(m_document->page(i));
        page->setImagePath("image://" + m_providerName + "/page/" + QString::number(i + 1));
        m_pages.push_back(page);
    }
    emit pagesChanged();
    emit loadedChanged();
    emit pagesCountChanged();
}

void PDFModel::search(const QString& text, Qt::CaseSensitivity caseSensitivity)
{
    m_threadSearch.setSearchParams(m_pages, text);
    m_threadSearch.start();
}

Transfer PDFModel::nextRect()
{
    QRectF temp = m_pages.at(*it)->nextRect();
    if(temp.isEmpty()){
        if(it + 1 == m_pageIndex.end())
            it = m_pageIndex.begin();
        else
            ++it;
        temp = m_pages.at(*it)->nextRect();
    }
    return Transfer(*it, temp.x(), temp.y(), temp.width(), temp.height());
}

Transfer PDFModel::previousRect()
{
    QRectF temp;
    temp = m_pages.at(*it)->previousRect();
    if(temp.isEmpty()){
        if(it == m_pageIndex.begin()){
            it = m_pageIndex.end();
        }
        --it;
        temp = m_pages.at(*it)->previousRect();
    }
    return Transfer(*it, temp.x(), temp.y(), temp.width(), temp.height());
}

void PDFModel::clearSearch()
{
    for(auto pageNumber: qAsConst(m_pageIndex)){
         m_pages.at(pageNumber)->clear();
    }
    m_pageIndex.clear();
}

void PDFModel::loadProvider()
{
    auto* engine = QQmlEngine::contextForObject(this)->engine();
    const QString& prefix = QString::number(quintptr(this));
    m_providerName = "poppler" + prefix;
    engine->addImageProvider(m_providerName, new ImageProvider(m_document));
}

int PDFModel::pagesCount()
{
    if(m_document != nullptr)
        return m_document->numPages();
    return 0;
}

void PDFModel::fillVectorIndex()
{
    for(int i = 0; i < m_pages.count(); ++i){
        if(m_pages.at(i)->countFoundRect() != 0){
            m_pageIndex.append(i);
        }
    }
    it = m_pageIndex.begin();
    emit searchFinished();
}
