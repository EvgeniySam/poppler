#include "threadsearch.h"

ThreadSearch::ThreadSearch(QObject *parent) : QThread(parent)
{

}

void ThreadSearch::setListPages(QList<Page *> &_listPages)
{
    m_listPages = _listPages;
}

void ThreadSearch::setSearchParams(QList<Page *> &_listPages, QString _text)
{
    m_listPages = _listPages;
    m_stringSearch = _text;
}

void ThreadSearch::run()
{
      for (const auto& r : qAsConst(m_listPages))
      {
          r->searchOnPage(m_stringSearch);
      }

      emit finished();
}
