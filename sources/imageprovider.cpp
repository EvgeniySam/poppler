#include "imageprovider.h"

ImageProvider::ImageProvider(Poppler::Document *pdfDocument):
    QQuickImageProvider(QQuickImageProvider::Image, QQmlImageProviderBase::ForceAsynchronousImageLoading),
    m_document(pdfDocument)
{

}

QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QTime t;
    t.start();

    QString type = id.section("/", 0, 0);
    QImage result;

    if (m_document && type == "page")
    {
      int numPage = id.section("/", 1, 1).toInt();
      QScopedPointer <Poppler::Page> page(m_document->page(numPage - 1));
      QSizeF pageSize = page->pageSizeF();
      double res = requestedSize.width() / (pageSize.width() / 72);
      result = page->renderToImage(res, res);
      *size = result.size();
    }
    return result;
}
