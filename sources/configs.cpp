#include <QSettings>
#include "configs.h"
#include <QDir>
#include <QFileInfo>

QString Configs::DirRoutes = "";
QString Configs::DirRegulatoryDocuments = "";
QString Configs::DirRaports = "";
QString Configs::logPath = "";

void loadSetting();

Configs::Configs(QObject *parent) :
    QObject(parent)
{
    QFont baseFont;
    baseFont.setWeight(400);
    baseFont.setFamily("Droid Sans");

    m_30Font = baseFont;
    m_30Font.setPixelSize(30 * m_scale);
    m_24Font = baseFont;
    m_24Font.setPixelSize(24 * m_scale);
    m_24BoldFont = m_24Font;
    m_24BoldFont.setBold(true);
    m_20Font = baseFont;
    m_20Font.setPixelSize(20 * m_scale);
    m_28Font = baseFont;
    m_28Font.setPixelSize(28 * m_scale);
    m_36Font = baseFont;
    m_36Font.setPixelSize(36 * m_scale);
    m_36Font.setWeight(700);
    m_48Font = baseFont;
    m_48Font.setWeight(700);
    m_48Font.setPixelSize(48 * m_scale);
    m_28BoldFont = m_28Font;
    m_28BoldFont.setBold(true);

    m_foregroundColor = QColor("#424242");
    m_backgroundColor = QColor("#FFFFFF");
    m_accentColor = QColor("#E65100");
    m_waringColor = QColor("#E31D24");
    m_assuageColor = QColor("#EEEEEE");
    m_disabledColor = QColor("#8D8C8C");
    m_disabledBackgroundColor = QColor("#E0DEDE");
    m_selectedItemColor = QColor("#FFCCBC");
    m_username = QString::fromLocal8Bit(qgetenv ("USER").constData());
    loadSetting();
}

QString Configs::userName()
{
    return m_username;
}


void Configs::loadSetting(){

    QString iniFileName = "pdf.ini";
    QDir path = QDir("/etc/pdf/");

    QSettings settings(path.absoluteFilePath(iniFileName), QSettings::IniFormat);
    DirRegulatoryDocuments = settings.value("DirRoutes", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    logPath = settings.value("logPath", "/var/log/pdf/pdf.log").toString();
}

