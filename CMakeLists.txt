cmake_minimum_required(VERSION 3.7)

project(docviewer LANGUAGES CXX VERSION 1.0.1)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_DEBUG "-O3 -ggdb3" CACHE STRING "")

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake;${CMAKE_MODULE_PATH}")

find_package(Qt5 COMPONENTS Core Quick QuickControls2 REQUIRED)

find_package(poppler-qt5)

set(HEADERS
    headers/fileinfo.h
    headers/configs.h
    headers/dirmanager.h
    headers/pdfmodel.h
    headers/imageprovider.h
    headers/log.h
    headers/page.h
    headers/threadsearch.h
    )
set(SOURCES
    sources/main.cpp
    sources/fileinfo.cpp
    sources/configs.cpp
    sources/dirmanager.cpp
    sources/pdfmodel.cpp
    sources/imageprovider.cpp
    sources/log.cpp
    sources/page.cpp
    sources/threadsearch.cpp
    )

set(RESOURCES
    resources/qml.qrc
    headers
    )

set(SOFTWARE_PACKAGE pdf)
set(DIR_UNPACKED /usr)


add_executable(${PROJECT_NAME}
  ${HEADERS}
  ${SOURCES}
  ${RESOURCES}
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
    headers
    )
target_compile_definitions(${PROJECT_NAME}
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
  PRIVATE
  Qt5::Core
  Qt5::Quick
  Qt5::QuickControls2
  poppler-qt5
  )

install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${DIR_UNPACKED}/bin
    COMPONENT ${PROJECT_NAME})

include(Package)



