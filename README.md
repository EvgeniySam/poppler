# Приложение просмтора pdf документов

Для сборки:
1. Склонировать репозиторий ``` git clone http://gitlabhead.rels.dom/armypatrol/marm-docviewer.git```
2. Создать директорию сборки
3. Из директории сборки выполнить:

    ```
    cmake .. 
    cpack -G DEB
    ```
 
# Пиктограммы приложений
    Скопировать содержимое `icons` в `/usr/share/pixmaps/`
